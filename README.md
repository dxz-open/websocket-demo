# websocket-demo

#### 介绍
springboot+websocket+sockjs进行消息推送demo

#### 软件架构

demo中包括了springboot+websocket+sockjs进行消息推送【基于STOMP协议】及SpringBoot集成WebSocket【基于纯H5】（非Stomp协议的原生WebSocket协议）两种实现方式。